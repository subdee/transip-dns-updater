<?php

namespace subdee\TransipDnsUpdater;

use TransIP\Client;
use TransIP\Model\DnsEntry;

class Updater
{
    private $config;
    private $client;
    private $ipGrabber;

    public function __construct(Client $client, Config $config, IpGrabber $ipGrabber)
    {
        $this->config = $config;
        $this->client = $client;
        $this->ipGrabber = $ipGrabber;
    }

    public function update()
    {
        try {
            $domainApi = $this->client->api('domain');
            $currentIp = $this->ipGrabber->getCurrentIp();

            /** @var DnsEntry[] $dnsEntries */
            $dnsEntries = $domainApi->getInfo($this->config->domain)->dnsEntries;

            $subdomains = explode(',', $this->config->subdomain);
            foreach ($subdomains as $subdomain) {
                $key = array_search($subdomain, array_column($dnsEntries, 'name'));

                if ($key === false) {
                    $dnsEntries[] = new DnsEntry(
                        $subdomain,
                        86400,
                        DnsEntry::TYPE_A,
                        $currentIp
                    );
                } elseif ($dnsEntries[$key]->type === DnsEntry::TYPE_A) {
                    $dnsEntries[$key]->content = $currentIp;
                }
            }

            return $domainApi->setDnsEntries($this->config->domain, $dnsEntries);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
